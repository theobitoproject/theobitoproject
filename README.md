<h1 align="center">The Obito Project</h1>
<p align="center">
  <img src="https://gitlab.com/uploads/-/system/user/avatar/13725671/avatar.png">
</p>

## About

The Obito Project is an initiative to help as many people as possible, specially children, in order to keep the essence of young Obito's spirit.

If you want to contribute to the Obito Project or you have an idea that might help people, feel free to reach out. Many lines of code are needed to make small contributions to the world.

## Projects

### Chiki

Calculate and visualize Z score to track children's growth.

Repositories:
- [Hiruzen](https://gitlab.com/the_chiki_project/hiruzen) (API)
- [Asuma](https://gitlab.com/the_chiki_project/asuma) (Front end)


### Kankuro

GO-SDK/CDK to build Airbyte connectors quickly in Golang.

The main goal is start building connectors for NGO's and little foundations that might benefit from data but they still relying on tools like Excel.

Repositories: 
- [Kankuro](https://gitlab.com/airbyte_with_go/kankuro)
- [Airbyte Destination Mongo with Go](https://gitlab.com/airbyte_with_go/airbyte_destination_mongo)
